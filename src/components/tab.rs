use crate::bootstrap::{get_tab_by_id, Tab};
use yew::prelude::*;
use yew::services;
use yew::services::ConsoleService;

#[derive(Properties, Clone, PartialEq, Debug)]
pub struct TabProps {
    #[prop_or(false)]
    pub active: bool,
    pub name: String,
    pub children: Children,
    pub id: String,
}

pub struct TabComponent {
    props: TabProps,
    link: ComponentLink<Self>,
    tab: Option<Tab>,
}

impl TabComponent {
    fn toggle(&self) {
        if self.props.active {
            // self.show()
        }
    }

    fn show(&self) {
        self.tab.as_ref().expect(&self.props.name).show();
    }
}

impl Component for TabComponent {
    type Message = ();
    type Properties = TabProps;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        TabComponent {
            props,
            link,
            tab: None,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        if self.props != props {
            self.props = props;
            self.toggle();
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        let mut class = if self.props.active {
            "tab-pane active"
        } else {
            "tab-pane"
        };

        html! {
        <div class={ class } id=self.props.id role="tabpanel">
            { self.props.children.clone() }
        </div>
        }
    }

    fn rendered(&mut self, first_render: bool) {
        if first_render {
            self.tab = Some(get_tab_by_id(&self.props.id));
        }
    }
}
