use crate::bootstrap::{get_collapse_by_id, Collapse};
use yew::prelude::*;
use yew::services;
use yew::services::ConsoleService;

const COLLAPSE_FORM_ID: &str = "collapseForm";

#[derive(Properties, Clone, PartialEq, Debug)]
pub struct CollapseProps {
    #[prop_or(false)]
    pub is_shown: bool,

    pub children: Children,
}

pub struct CollapseComponent {
    props: CollapseProps,
    link: ComponentLink<Self>,
    collapse: Option<Collapse>,
}

impl CollapseComponent {
    fn toggle(&self) {
        if self.props.is_shown {
            self.show()
        } else {
            self.hide()
        }
    }

    fn show(&self) {
        self.collapse.as_ref().expect(COLLAPSE_FORM_ID).show();
    }

    fn hide(&self) {
        self.collapse.as_ref().expect(COLLAPSE_FORM_ID).hide();
    }
}

impl Component for CollapseComponent {
    type Message = ();
    type Properties = CollapseProps;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        CollapseComponent {
            props,
            link,
            collapse: None,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        if self.props != props {
            self.props = props;
            self.toggle();
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        html! {
        <div class="collapse" id="collapseForm">
            { self.props.children.clone() }
        </div>
        }
    }

    fn rendered(&mut self, first_render: bool) {
        if first_render {
            self.collapse = Some(get_collapse_by_id(COLLAPSE_FORM_ID));
        }
    }
}
