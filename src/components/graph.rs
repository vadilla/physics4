use crate::graph_renderer;
use std::future::Future;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::HtmlCanvasElement;
use yew::prelude::*;
use yew::services::ConsoleService;
use crate::app::{ModelState, ModelOptions};

pub struct Graph {
    canvas_ref: NodeRef,
    link: ComponentLink<Self>,
    props: GraphProps,
}

#[derive(Properties, PartialEq, Clone, Debug)]
pub struct GraphProps {
    pub state: Option<ModelState>,
    pub options: ModelOptions,
    pub labels: (String, String),
}

impl Graph {
    fn redraw_canvas(&self, state: &ModelState) {
        let canvas_opt: Option<HtmlCanvasElement> = self.canvas_ref.cast();
        if let Some(canvas) = canvas_opt {
            graph_renderer::draw(
                state,
                &self.props.options,
                canvas,
                &self.props.labels,
            );
        }
    }
}

impl Component for Graph {
    type Message = ();
    type Properties = GraphProps;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Graph {
            canvas_ref: NodeRef::default(),
            link,
            props,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        if self.props != props {
            self.props = props;
            if let Some(state) = &self.props.state {
                self.redraw_canvas(state)
            }
            true
        } else {
            true
        }
    }

    fn view(&self) -> Html {
        html! {
            // <div class="row">
                <canvas ref=self.canvas_ref.clone() class="col-md-8" />
            // </div>
        }
    }

    fn rendered(&mut self, first_render: bool) {
        if first_render {
            let canvas_opt: Option<HtmlCanvasElement> = self.canvas_ref.cast();
            if let Some(canvas) = canvas_opt {
                let rect = canvas.get_bounding_client_rect();
                let (w, h) = (rect.width(), rect.height());
                canvas.set_width(w as u32);
                canvas.set_height(h as u32);

            }

            if let Some(state) = &self.props.state {
                self.redraw_canvas(state)
            }
        }
    }
}
