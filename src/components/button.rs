use yew::prelude::*;
use yew::services;
use yew::services::ConsoleService;


#[derive(Properties, Clone, PartialEq, Debug)]
pub struct ButtonProps {
    #[prop_or_default]
    pub title: String,
    pub on_press: Callback<()>,

    #[prop_or("btn-primary".into())]
    pub kind: String,
}

pub struct ButtonComponent {
    link: ComponentLink<Self>,
    title: String,
    on_press: Callback<()>,
    kind: String,
}

pub enum Msg {
    Clicked,
}

impl Component for ButtonComponent {
    type Message = Msg;
    type Properties = ButtonProps;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        ButtonComponent {
            link,
            title: props.title,
            on_press: props.on_press,
            kind: props.kind,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Clicked => {
                self.on_press.emit(());
            }
        }
        false
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        let class = format!("btn {} me-1", self.kind);
        html! {
            <button type="button" class={ class } onclick=self.link.callback(|_| Msg::Clicked)>
                { &self.title }
            </button>
        }
    }
}
