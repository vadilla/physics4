use yew::prelude::*;
use yew::services;
use yew::services::ConsoleService;

#[derive(Properties, Clone, PartialEq, Debug)]
pub struct InputProps {
    #[prop_or_default]
    pub title: String,
    pub measure: String,
    pub value: i32,
    pub on_change: Callback<i32>,
}

pub struct InputComponent {
    link: ComponentLink<Self>,
    title: String,
    measure: String,
    on_change: Callback<i32>,
    value: String,
    valid: bool,
}

pub enum Msg {
    Change(String),
}

impl Component for InputComponent {
    type Message = Msg;
    type Properties = InputProps;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        InputComponent {
            link,
            title: props.title,
            measure: props.measure,
            on_change: props.on_change,
            valid: true,
            value: props.value.to_string(),
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Change(value) => {
                self.value = value;
                if let Ok(x) = self.value.parse() {
                    self.valid = true;
                    self.on_change.emit(x);
                } else {
                    self.valid = false;
                }
            }
        }
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        let class = if self.valid {
            "form-control"
        } else {
            "form-control is-invalid"
        };

        let measure_span =
            if self.measure.is_empty()
            { html! {} } else { html! { <span class="input-group-text" id="basic-addon3">{ &self.measure }</span>} };

        html! {
        <>
            <label class="form-label">{ &self.title }</label>
            <div class="input-group mb-3">
                <input oninput=self.link.callback(|e: InputData| Msg::Change(e.value))
                type="text" class={ class } aria-describedby="basic-addon3" value={ &self.value }/>
                { measure_span }

            </div>
        </>
        }
    }
}
