// use crate::app::{ModelOptions, ModelState};
// use wasm_bindgen::{JsCast, JsValue};
// use yew::prelude::*;
// use yew::services::ConsoleService;
//
// pub struct CanvasComponent {
//     link: ComponentLink<Self>,
//     context: Option<web_sys::CanvasRenderingContext2d>,
//     props: CanvasProps,
//     size: (u32, u32),
// }
//
// #[derive(Properties, PartialEq, Clone, Debug)]
// pub struct CanvasProps {
//     pub model_state: ModelState,
//     pub model_options: ModelOptions,
//     // pub trace_pool: DataPool<ModelState>,
// }
//
// pub enum Msg {
//     Draw,
// }
//
// impl Component for CanvasComponent {
//     type Message = Msg;
//     type Properties = CanvasProps;
//
//     fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
//         CanvasComponent {
//             link,
//             props,
//             context: None,
//             size: (0, 0),
//         }
//     }
//
//     fn update(&mut self, msg: Self::Message) -> bool {
//         false
//     }
//
//     fn change(&mut self, props: Self::Properties) -> bool {
//         if self.props != props {
//             self.props = props;
//             self.draw();
//             true
//         } else {
//             false
//         }
//     }
//
//     fn view(&self) -> Html {
//         html! {
//             <canvas id="canvas" class="col-md-8" />
//         }
//     }
//
//     fn rendered(&mut self, first_render: bool) {
//         if first_render {
//             let window = web_sys::window().unwrap();
//             let document = window.document().unwrap();
//             let canvas = document.get_element_by_id("canvas").unwrap();
//             let canvas: web_sys::HtmlCanvasElement = canvas
//                 .dyn_into::<web_sys::HtmlCanvasElement>()
//                 .map_err(|_| ())
//                 .unwrap();
//
//             let dpi = window.device_pixel_ratio();
//             let rect = canvas.get_bounding_client_rect();
//             let (w, h) = (rect.width(), rect.height());
//
//             canvas.set_width((w * dpi) as u32);
//             canvas.set_height((h * dpi) as u32);
//
//             let context = canvas
//                 .get_context("2d")
//                 .unwrap()
//                 .unwrap()
//                 .dyn_into::<web_sys::CanvasRenderingContext2d>()
//                 .unwrap();
//
//             context.scale(dpi, dpi);
//
//             canvas
//                 .style()
//                 .set_property("width", &format!("{}px", w))
//                 .unwrap();
//             canvas
//                 .style()
//                 .set_property("height", &format!("{}px", h))
//                 .unwrap();
//
//             self.context = Some(context);
//             self.size =
//                 (canvas.width() / dpi as u32, canvas.height() / dpi as u32);
//         }
//         self.draw();
//     }
// }
//
// impl CanvasComponent {
//     fn draw_box(&self) {}
//
//     fn draw(&self) {
//         // let ctx = self.context.as_ref().unwrap();
//         // let (x, angle) = (
//         //     self.props.model_state.x * 100.0,
//         //     self.props.model_state.angle,
//         // );
//         // let (w, h) = (self.size.0 as f64, self.size.1 as f64);
//         // let l = self.props.model_options.l * 100.0;
//         //
//         // let (rect_w, rect_h) = (100.0, 50.0);
//         // let (rect_x, rect_y) = (w / 2.0, 30.0);
//         //
//         // let radius = 30.0;
//         //
//         // ctx.begin_path();
//         // ctx.clear_rect(0.0, 0.0, w, h);
//         //
//         // ctx.rect(rect_x - rect_w / 2.0 + x, rect_y, rect_w, rect_h);
//         //
//         // let circle_x = x + l * angle.sin();
//         // let circle_y = l * angle.cos();
//         //
//         // ctx.move_to(rect_x + x, rect_y + rect_h / 2.0);
//         // ctx.line_to(rect_x + circle_x, circle_y + rect_y + rect_h / 2.0);
//         // ctx.stroke();
//         //
//         // ctx.begin_path();
//         // ctx.set_fill_style(&JsValue::from("BLACK"));
//         // ctx.arc(
//         //     rect_x + circle_x,
//         //     circle_y + rect_y + rect_h / 2.0,
//         //     radius,
//         //     0.0,
//         //     3.14 * 2.0,
//         // );
//         // ctx.fill();
//         // ctx.begin_path();
//         //
//         // ctx.set_fill_style(&JsValue::from("white"));
//         // ctx.arc(
//         //     rect_x + circle_x,
//         //     circle_y + rect_y + rect_h / 2.0,
//         //     radius / 10.0,
//         //     0.0,
//         //     3.14 * 2.0,
//         // );
//         //
//         // ctx.fill();
//         //
//         // if self.props.trace_pool.data.is_empty() {
//         //     return;
//         // }
//         // // ConsoleService::log(&format!("{:?}", &self.props.trace_pool));
//         //
//         // let first = &self.props.trace_pool.data.first().as_ref().unwrap().1;
//         // let (x, angle) = (first.x * 100.0, first.angle);
//         // let circle_x = x + l * angle.sin();
//         // let circle_y = l * angle.cos();
//         // ctx.begin_path();
//         //
//         // ctx.move_to(rect_x + circle_x, circle_y + rect_y + rect_h / 2.0);
//         //
//         // let first_t = &self.props.trace_pool.data.first().as_ref().unwrap().0;
//         //
//         // for state in self.props.trace_pool.data.iter() {
//         //     let first = &state.1;
//         //
//         //     let (x, angle) = (first.x * 100.0, first.angle);
//         //     let circle_x = x + l * angle.sin();
//         //     let circle_y = l * angle.cos();
//         //
//         //     let k = (state.0 - first_t) / self.props.trace_pool.range;
//         //
//         //     ctx.move_to(rect_x + circle_x, circle_y + rect_y + rect_h / 2.0);
//         //     ctx.arc(
//         //         rect_x + circle_x,
//         //         circle_y + rect_y + rect_h / 2.0,
//         //         0.5 + 3.0 * k,
//         //         0.0,
//         //         3.14 * 2.0,
//         //     );
//         // }
//         // ctx.set_fill_style(&JsValue::from("rgba(255, 100, 255, 0.5)"));
//         // ctx.fill();
//         // ctx.set_stroke_style(&JsValue::from("BLACK"));
//     }
// }
