use plotters::prelude::*;
use plotters::prelude::*;
use plotters_canvas::CanvasBackend;
use web_sys::HtmlCanvasElement;
use crate::app::{ModelState, ModelOptions};

// TODO: create an appropriate error type
pub fn draw(
    state: &ModelState,
    options: &ModelOptions,
    el: HtmlCanvasElement,
    labels: &(String, String),
) {
    let backend = CanvasBackend::with_canvas_object(el)
        .expect("unable to retrieve canvas context");

    let root = backend.into_drawing_area();

    // if data_pool.data.is_empty() {
    //     return
    // }

    let first = -0.013;
    let last = 0.013;

    root.fill(&WHITE);
    let mut chart = ChartBuilder::on(&root)
        .x_label_area_size(50)
        .y_label_area_size(70)
        .margin(23)
        .build_cartesian_2d(first as f32..last as f32, 0f32..1.05 * options.n.powf(2.0) as f32)
        .expect("");

    chart
        .configure_mesh()
        .y_desc(&labels.0)
        .x_desc(&labels.1)
        .axis_desc_style(("sans-serif", 15))
        .draw();


    chart
        .draw_series(LineSeries::new(
            state.x.iter().zip(state.y.iter()).map(|(t, x)| (*t as f32, *x as f32)),
            &RED,
        ))
        .unwrap()
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &RED));

    if options.show {
        chart
            .draw_series(LineSeries::new(
                state.x.iter().zip(state.y_inter.iter()).map(|(t, x)| (*t as f32, *x as f32)),
                &BLUE,
            ))
            .unwrap()
            .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &RED));
    }
    root.present();
}
