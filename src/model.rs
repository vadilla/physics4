use crate::app::{ModelOptions, ModelState};
use std::time;
use std::time::Instant;

pub struct Model {
    options: ModelOptions,
    state: Option<ModelState>,
}

impl Model {
    pub fn new(options: ModelOptions) -> Self {
        Model {
            options,
            state: None,
        }
    }

    fn i_diff(&self, sinfi: f64) -> f64 {
        let u = std::f64::consts::PI * self.options.a * sinfi / self.options.l;
        return (u.sin() / u).powf(2.0);
    }

    fn i_inter(&self, sinfi: f64) -> f64 {
        let delta = std::f64::consts::PI * (self.options.a + self.options.b) * sinfi / self.options.l;
        return ((self.options.n * delta).sin() / delta.sin()).powf(2.0);
    }

    pub fn calculate(&mut self, options: ModelOptions) -> ModelState {
        self.options = options;
        let precision = 100000.0;
        let x =
            ((self.options.range.0 * precision) as i32..(self.options.range.1 * precision) as i32)
                .filter(|x| x != &0)
                .map(|x| x as f64 / precision);
        let y_inter = x.clone().map(|x| self.i_diff(x) *self.options.n.powf(2.0));
        let y = x.clone().map(|x| self.i_diff(x) * self.i_inter(x));

        ModelState {
            x: x.collect(),
            y_inter: y_inter.collect(),
            y: y.collect(),
        }
    }
}
