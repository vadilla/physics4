use yew::prelude::*;

use super::components::button::ButtonComponent;
use super::components::collapse::CollapseComponent;
use super::components::graph::Graph;
use super::components::input::InputComponent;
use super::components::slider::SliderComponent;
use super::components::switch::SwitchComponent;
use super::components::tab::TabComponent;
use super::components::tab_button::TabButtonComponent;
use super::model::Model;

use wasm_bindgen::__rt::std::time::{Duration, Instant};
use yew::services::interval::IntervalTask;
use yew::services::render::RenderTask;
use yew::services::{ConsoleService, IntervalService, RenderService};
use crate::app::Change::{HoleCount, Lambda, HoleWidth, BetweenWidth, Period};
use std::cmp::min;

pub struct App {
    link: ComponentLink<Self>,
    model: Model,
    model_state: Option<ModelState>,
    model_options: ModelOptions,
    show_inter: bool,
}

pub enum AppMessage {
    Change(Change),
    Switch,
    Render,
}

pub enum Change {
    HoleCount(i32),
    Lambda(i32),
    HoleWidth(i32),
    BetweenWidth(i32),
    Period(i32),
}


#[derive(PartialEq, Clone, Debug)]
pub struct ModelState {
    pub x: Vec<f64>,
    pub y_inter: Vec<f64>,
    pub y: Vec<f64>,
}

#[derive(PartialEq, Clone, Debug)]
pub struct ModelOptions {
    pub a: f64,
    pub b: f64,
    pub l: f64,
    pub n: f64,
    pub range: (f64, f64),
    pub period: f64,
    pub show: bool,
}

impl Component for App {
    type Message = AppMessage;
    type Properties = ();

    fn create(_props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let default_options = ModelOptions {
            a: 54.0 * 1e-6,
            b: 112.0 * 1e-6,
            l: 500.0 * 1e-9,
            n: 4.0,
            range: (-0.013, 0.013),
            period: 166.0 * 1e-6,
            show: false,
        };

        App {
            link,
            // model_state: model.state().clone(),
            model: Model::new(default_options.clone()),
            model_options: default_options,
            model_state: None,
            show_inter: false,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            AppMessage::Change(change) => {
                match change {
                    HoleCount(n) => {
                        self.model_options.n = n as f64
                    }
                    Lambda(l) => {
                        self.model_options.l = l as f64 * 1e-9
                    }
                    HoleWidth(a) => {
                        self.model_options.a = a as f64 * 1e-6;
                        self.model_options.period = self.model_options.a + self.model_options.b
                    }
                    BetweenWidth(b) => {
                        self.model_options.b = b as f64 * 1e-6;
                        self.model_options.period = self.model_options.a + self.model_options.b
                    }
                    Period(p) => {
                        let p = p as f64 * 1e-6;
                        let k = self.model_options.period / p;
                        self.model_options.a = f64::min(self.model_options.a / k, 400.0 * 1e-6);
                        self.model_options.b = f64::min(self.model_options.b / k, 400.0 * 1e-6);
                        self.model_options.a = f64::max(1e-6, self.model_options.a);
                        self.model_options.b = f64::max(1e-6, self.model_options.b);
                        self.model_options.period = self.model_options.a + self.model_options.b;
                    }
                }
                self.model_state = Some(self.model.calculate(self.model_options.clone()));
                return true
            }
            AppMessage::Render => {
                return true
            }

            AppMessage::Switch => {
                self.show_inter ^= true;
                self.model_options.show = self.show_inter;
                return true
            }
        }
        false
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        let hole_count_change = self.link.callback(|x| AppMessage::Change(HoleCount(x)));
        let lambda_change = self.link.callback(|x| AppMessage::Change(Lambda(x)));
        let hole_width_change = self.link.callback(|x| AppMessage::Change(HoleWidth(x)));
        let between_width_change = self.link.callback(|x| AppMessage::Change(BetweenWidth(x)));
        let period_change = self.link.callback(|x| AppMessage::Change(Period(x)));

        let on_switch = self.link.callback(|_| AppMessage::Switch);

        html! {
        <>
            <div class="container h-auto">
                <div class="row">
                    <div class="col">
                        <div class="card d-flex flex-column mb-3" style="min-height: 100%">
                            <div class="row no-gutters flex-fill" style="min-height: 400px">
                                <div class="col-md-4">
                                    <div class="card-body">
                                        <InputComponent title={"Количество щелей"}
                                        on_change={ &hole_count_change } measure="" value={ (self.model_options.n) as i32  } />
                                        <InputComponent title={"Длина волны"}
                                        on_change={ &lambda_change } measure="нм" value={ (self.model_options.l * 1e9) as i32  } />

                                        <SliderComponent title={"Ширина щели"}
                                        on_change={ &hole_width_change } range={(1,400)} value={ (self.model_options.a * 1e6) as i32 }/>
                                        <SliderComponent title={"Ширина промежутка"}
                                        on_change={ &between_width_change } range={(1,400)} value={ (self.model_options.b * 1e6) as i32 }/>
                                        <SliderComponent title={"Период решетки"}
                                        on_change={ &period_change } range={(2,800)} value={ (self.model_options.period * 1e6) as i32 }/>

                                        <SwitchComponent
                                            title={"Показать дифракционную составляющую"}
                                            on_press={ &on_switch }
                                            checked={ &self.show_inter }
                                        />

                                        <p class="mt-2 mb-2 text-muted">
                                            { "Расстояние от решетки до экрана: 1 метр" }
                                        </p>

                                        </div>
                                    </div>
                                    <Graph
                                        options={ &self.model_options }
                                        state={ &self.model_state }
                                        labels={ ("Интенсивность I Вт/м²".into(), "Координата X м".into()) }
                                    />
                                </div>
                                // <CanvasComponent model_state={ &self.model_state }
                                // model_options={ &self.model_options } trace_pool={ &self.trace_pool }/>
                            </div>
                        </div>
                    </div>
                </div>
        </>
        }
    }

    fn rendered(&mut self, first_render: bool) {
        if first_render {
            self.model_state = Some(self.model.calculate(self.model_options.clone()));
            self.link.send_message(AppMessage::Render)
        }
    }
}
