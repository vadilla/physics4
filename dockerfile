FROM rust as rust-build-stage
RUN cargo install wasm-pack
COPY src/ src/
COPY Cargo.* ./
COPY index.* ./
RUN ["wasm-pack", "build", "--target", "web", "--out-name", "app"]

FROM node:lts-alpine as npm-build-stage
WORKDIR /app
COPY --from=rust-build-stage pkg/ pkg/
COPY index.* ./
COPY webpack.* ./
COPY package* ./

RUN npm install
RUN npm run build

FROM nginx:stable-alpine as production-stage
COPY --from=npm-build-stage /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

